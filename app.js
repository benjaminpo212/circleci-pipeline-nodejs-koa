const Koa = require('koa');

const app = new Koa();
const port = process.env.PORT || 3000;

app.use(async (ctx) => {
  ctx.body = 'Hello World!';
});

// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Listening on port ${port}!`));

module.exports = app;
