FROM node:13.5.0-alpine
WORKDIR /usr/src/app
COPY package.json package*.json ./
RUN yarn install --only=production
COPY . .
CMD [ "yarn", "start" ]
