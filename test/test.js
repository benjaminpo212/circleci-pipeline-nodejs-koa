/* eslint-disable no-undef */

const server = require('../app').listen();
const request = require('supertest').agent(server);

describe('Hello World!', () => {
  after(() => {
    server.close();
  });

  it('should say "Hello World!"', (done) => {
    request
      .get('/')
      .expect(200)
      .expect('Hello World!', done);
  });
});
